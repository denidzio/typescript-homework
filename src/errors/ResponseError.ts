import IResponseError from '../interfaces/IResponseError';

export default class ResponseError extends Error implements IResponseError {
    public status_message: string;
    public status_code: number;

    constructor(status_message: string, status_code: number) {
        super();

        this.name = 'ResponseError';

        this.status_message = status_message;
        this.status_code = status_code;
    }
}
