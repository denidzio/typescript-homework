import { IMovie } from '../interfaces';
import renderMovieCard from './movieCard.dom';

const nodes = {
    container: document.getElementById('favorite-movies'),
};

const renderFavouriteMovie = async (movie: IMovie): Promise<HTMLElement> => {
    const classNames = ['col-12', 'p-2'];
    const node = document.createElement('div');

    node.classList.add(...classNames);
    node.append(await renderMovieCard(movie, true));

    return node;
};

const render = async (movies: IMovie[]): Promise<void> => {
    if (!nodes.container) {
        return;
    }

    const movieNodes = [];

    for (const movie of movies) {
        movieNodes.push(await renderFavouriteMovie(movie));
    }

    nodes.container.innerHTML = '';
    nodes.container.append(...movieNodes);
};

export default render;
