import { IMovie } from '../interfaces';
import { random } from '../helpers/random.helper';

const nodes = {
    container: document.getElementById('random-movie'),
    title: document.getElementById('random-movie-name'),
    description: document.getElementById('random-movie-description'),
};

const render = (movies: IMovie[]): void => {
    if (!nodes.container || !nodes.title || !nodes.description) {
        return;
    }

    if (!movies || !movies.length) {
        return;
    }

    const randomMovieIndex = random(0, movies.length - 1);
    const randomMovie = movies[randomMovieIndex];

    if (!randomMovie.backdropPath) {
        render(movies);
        return;
    }

    nodes.container.setAttribute(
        'style',
        `background-image: url(https://image.tmdb.org/t/p/original${randomMovie.backdropPath})`
    );

    nodes.title.innerHTML = randomMovie.title;
    nodes.description.innerHTML = randomMovie.overview;
};

export default render;
