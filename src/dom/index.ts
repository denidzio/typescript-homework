export { default as renderMovieCard } from './movieCard.dom';
export { default as renderMovies } from './movie.dom';
export { default as renderFavouriteMovies } from './favouriteMovie.dom';
export { default as renderRandomMovie } from './randomMovie.dom';
export { default as renderAlbum } from './album.dom';
