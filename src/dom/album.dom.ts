import { IAlbum } from '../interfaces';
import { START_FROM_PAGE } from '../constants';
import { renderMovies } from './';
import { loadMore } from '../listeners';
import { removeEventListeners } from '../helpers/dom.helper';
import { getFavouriteMoviesId } from '../helpers/movie.helper';

const render = async (
    getAlbum: (page: number) => Promise<IAlbum>
): Promise<void> => {
    const loadMoreButton = document.getElementById('load-more');
    removeEventListeners(loadMoreButton);

    const album = await getAlbum(START_FROM_PAGE);
    renderMovies(album.movies, getFavouriteMoviesId());

    loadMore.listen(album, getAlbum);
};

export default render;
