import { IMovie } from '../interfaces';
import renderMovieCard from './movieCard.dom';

const nodes = {
    container: document.getElementById('film-container'),
};

const renderMovie = async (
    movie: IMovie,
    isFavourite: boolean
): Promise<HTMLElement> => {
    const classNames = ['col-lg-3', 'col-md-4', 'col-12', 'p-2'];
    const node = document.createElement('div');

    node.classList.add(...classNames);
    node.append(await renderMovieCard(movie, isFavourite));

    return node;
};

const render = async (
    movies: IMovie[],
    favourites: number[]
): Promise<void> => {
    if (!nodes.container) {
        return;
    }

    const movieNodes = [];

    for (const movie of movies) {
        movieNodes.push(
            await renderMovie(movie, favourites.includes(movie.id))
        );
    }

    nodes.container.innerHTML = '';
    nodes.container.append(...movieNodes);
};

export default render;
