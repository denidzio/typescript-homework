import { IMovie } from '../interfaces';
import { movieCard } from '../listeners';
import { loadImageAsync } from '../helpers/dom.helper';

const render = async (
    movie: IMovie,
    isFavourite: boolean
): Promise<HTMLElement> => {
    const node = document.createElement('div');

    node.innerHTML = `
        <div class="card shadow-sm">
            <img
                src="${
                    movie.posterPath
                        ? `https://image.tmdb.org/t/p/original${movie.posterPath}`
                        : ''
                }"
            />
            <svg
                xmlns="http://www.w3.org/2000/svg"
                class="bi bi-heart-fill position-absolute p-2 movie-favourite ${
                    isFavourite ? 'movie-favourite_active' : ''
                }"
                viewBox="0 -2 18 22"
            >
            <path
                fill-rule="evenodd"
                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
            />
            </svg>
            <div class="card-body">
                <p class="card-text truncate">
                    ${movie.overview}
                </p>
                <div class="d-flex justify-content-between align-items-center">
                    <small class="text-muted">${movie.date}</small>
                </div>
            </div>
        </div>
    `;

    const movieNode = node.firstElementChild as HTMLElement;

    movieCard.listen(movie, movieNode);

    if (movie.posterPath) {
        const movieImg = movieNode.querySelector('img') as HTMLImageElement;
        await loadImageAsync(movieImg);
    }

    return movieNode;
};

export default render;
