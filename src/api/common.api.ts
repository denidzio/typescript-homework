import { API_KEY } from '../constants';
import ResponseError from '../errors/ResponseError';

import { IRequest } from '../interfaces';

const request = async <T>(request: IRequest): Promise<T> => {
    const { url, method, body } = request;

    const headers = {
        'Content-Type': 'application/json;charset=utf-8',
        Authorization: `Bearer ${API_KEY}`,
    };

    const response = await fetch(url, {
        method,
        headers,
        body: JSON.stringify(body),
    });

    if (!response.ok) {
        throw new ResponseError(response.statusText, response.status);
    }

    return await response.json();
};

export const get = async <T>(url: string): Promise<T> => {
    return await request({ url, method: 'GET' });
};
