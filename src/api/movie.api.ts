import ResponseError from '../errors/ResponseError';
import { IMovieResponse, IResponseMovie } from '../interfaces';

import { get } from './common.api';

export const getMovieByName = async (
    name: string,
    page: number
): Promise<IMovieResponse | ResponseError> => {
    try {
        return await get<IMovieResponse>(
            `https://api.themoviedb.org/3/search/movie?query=${encodeURI(
                name
            )}&language=en-US&page=${page}`
        );
    } catch (error) {
        return error as ResponseError;
    }
};

export const getPopularMovies = async (
    page: number
): Promise<IMovieResponse | ResponseError> => {
    try {
        return await get<IMovieResponse>(
            `https://api.themoviedb.org/3/movie/popular?language=en-US&page=${page}`
        );
    } catch (error) {
        return error as ResponseError;
    }
};

export const getTopRatedMovies = async (
    page: number
): Promise<IMovieResponse | ResponseError> => {
    try {
        return await get<IMovieResponse>(
            `https://api.themoviedb.org/3/movie/top_rated?language=en-US&page=${page}`
        );
    } catch (error) {
        return error as ResponseError;
    }
};

export const getUpcomingMovies = async (
    page: number
): Promise<IMovieResponse | ResponseError> => {
    try {
        return await get<IMovieResponse>(
            `https://api.themoviedb.org/3/movie/upcoming?language=en-US&page=${page}`
        );
    } catch (error) {
        return error as ResponseError;
    }
};

export const getMovieDetailsById = async (
    id: string | number
): Promise<IResponseMovie | ResponseError> => {
    try {
        return await get<IResponseMovie | ResponseError>(
            `https://api.themoviedb.org/3/movie/${id}?language=en-US`
        );
    } catch (error) {
        return error as ResponseError;
    }
};
