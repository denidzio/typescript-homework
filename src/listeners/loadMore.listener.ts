import { renderMovies } from '../dom';
import { getFavouriteMoviesId } from '../helpers/movie.helper';
import { IAlbum } from '../interfaces';

const listen = async (
    album: IAlbum,
    getAlbum: (page: number) => Promise<IAlbum>
): Promise<void> => {
    const node = document.getElementById('load-more');

    node?.addEventListener('click', async () => {
        if (album.currentPage === album.allPages) {
            return;
        }

        const nextPageAlbum = await getAlbum(album.currentPage + 1);

        album.currentPage = nextPageAlbum.currentPage;
        album.movies = [...album.movies, ...nextPageAlbum.movies];

        renderMovies(album.movies, getFavouriteMoviesId());
    });
};

export default { listen };
