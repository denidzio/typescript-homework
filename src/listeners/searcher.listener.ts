import { getMovieByName } from '../services/movie.service';
import { renderAlbum } from '../dom/';

const nodes = {
    form: document.forms[0],
};

const listen = (): void => {
    nodes.form?.addEventListener('submit', async (e) => {
        e.preventDefault();

        const searchQuery = nodes.form.search.value.trim();

        if (searchQuery === '') {
            return;
        }

        renderAlbum(async (page) => await getMovieByName(searchQuery, page));

        const checkedCategorie = document.querySelector(
            'input[name=btnradio][checked]'
        ) as HTMLInputElement;

        if (checkedCategorie) {
            checkedCategorie.checked = false;
        }

        nodes.form.search.value = '';
    });
};

export default { listen };
