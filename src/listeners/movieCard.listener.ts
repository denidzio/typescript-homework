import { IMovie } from '../interfaces';

import {
    addFavouriteMovie,
    removeFavouriteMovie,
} from '../helpers/movie.helper';

const listen = (movie: IMovie, movieNode: HTMLElement | null): void => {
    movieNode?.addEventListener('click', (e) => {
        const target = e.target as HTMLElement | null;

        if (!target) {
            return;
        }

        const likeNode = target.closest('svg');

        if (!likeNode) {
            return;
        }

        if (likeNode.classList.contains('movie-favourite_active')) {
            removeFavouriteMovie(movie.id);
        } else {
            addFavouriteMovie(movie.id);
        }

        likeNode.classList.toggle('movie-favourite_active');
    });
};

export default { listen };
