import { renderAlbum } from '../dom';
import {
    getPopularMovies,
    getUpcomingMovies,
    getTopRatedMovies,
} from '../services/movie.service';

const nodes = {
    popular: document.getElementById('popular'),
    upcoming: document.getElementById('upcoming'),
    topRated: document.getElementById('top_rated'),
};

const listen = (): void => {
    nodes.popular?.addEventListener('change', async () => {
        renderAlbum(async (page) => await getPopularMovies(page));
    });

    nodes.upcoming?.addEventListener('change', async () => {
        renderAlbum(async (page) => await getUpcomingMovies(page));
    });

    nodes.topRated?.addEventListener('change', async () => {
        renderAlbum(async (page) => await getTopRatedMovies(page));
    });
};

export default { listen };
