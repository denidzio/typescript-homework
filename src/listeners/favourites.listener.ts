import renderFavouriteMovies from '../dom/favouriteMovie.dom';
import { getFavouriteMovies } from '../services/movie.service';
import { getFavouriteMoviesId } from '../helpers/movie.helper';

const nodes = {
    toggler: document.getElementById('open-favourites'),
};

const listen = (): void => {
    nodes.toggler?.addEventListener('click', async () => {
        const favouriteMovies = await getFavouriteMovies(
            getFavouriteMoviesId()
        );

        renderFavouriteMovies(favouriteMovies);
    });
};

export default { listen };
