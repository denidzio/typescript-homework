import ResponseError from '../errors/ResponseError';
import { START_FROM_PAGE } from '../constants';
import { IMovie, IAlbum } from '../interfaces';
import {
    getMovieByName as getMovieByNameApi,
    getPopularMovies as getPopularMoviesApi,
    getTopRatedMovies as getTopRatedMoviesApi,
    getUpcomingMovies as getUpcomingMoviesApi,
    getMovieDetailsById as getMovieDetailsByIdApi,
} from '../api/movie.api';
import { mapAlbum } from '../helpers/album.helper';
import { mapMovie } from '../helpers/movie.helper';

const emptyAlbum: IAlbum = {
    currentPage: 1,
    allPages: 1,
    movies: [],
};

export const getMovieByName = async (
    name: string,
    page = START_FROM_PAGE
): Promise<IAlbum> => {
    const movies = await getMovieByNameApi(name, page);

    if (movies instanceof ResponseError) {
        return emptyAlbum;
    }

    return mapAlbum(movies);
};

export const getPopularMovies = async (
    page = START_FROM_PAGE
): Promise<IAlbum> => {
    const movies = await getPopularMoviesApi(page);

    if (movies instanceof ResponseError) {
        return emptyAlbum;
    }

    return mapAlbum(movies);
};

export const getTopRatedMovies = async (
    page = START_FROM_PAGE
): Promise<IAlbum> => {
    const movies = await getTopRatedMoviesApi(page);

    if (movies instanceof ResponseError) {
        return emptyAlbum;
    }

    return mapAlbum(movies);
};

export const getUpcomingMovies = async (
    page = START_FROM_PAGE
): Promise<IAlbum> => {
    const movies = await getUpcomingMoviesApi(page);

    if (movies instanceof ResponseError) {
        return emptyAlbum;
    }

    return mapAlbum(movies);
};

export const getFavouriteMovies = async (ids: number[]): Promise<IMovie[]> => {
    const promises = ids.map((id) => getMovieDetailsByIdApi(id));
    const data = await Promise.all(promises);

    const movies: IMovie[] = [];

    for (const item of data) {
        if (item instanceof ResponseError) {
            continue;
        }

        movies.push(mapMovie(item));
    }

    return movies;
};
