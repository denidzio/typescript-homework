export { default as IMovieResponse } from './IMovieResponse';
export { default as IResponseMovie } from './IResponseMovie';
export { default as IDictionary } from './IDictionary';
export { default as IMovie } from './IMovie';
export { default as IRequest } from './IRequest';
export { default as IAlbum } from './IAlbum';
