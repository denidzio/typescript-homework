export default interface IMovie {
    id: number;
    overview: string;
    title: string;
    date: string;
    posterPath: string | null;
    backdropPath: string | null;
}
