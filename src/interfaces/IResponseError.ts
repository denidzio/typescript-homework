export default interface IResponseError {
    status_message: string;
    status_code: number;
}
