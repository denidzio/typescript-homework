import { IDictionary } from './';

export default interface IRequest {
    url: string;
    method: string;
    body?: IDictionary<unknown>;
}
