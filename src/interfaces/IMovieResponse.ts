import { IResponseMovie } from './';

export default interface IMovieResponse {
    page: number;
    results: IResponseMovie[];
    total_pages: number;
    total_results: number;
}
