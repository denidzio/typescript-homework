import IMovie from './IMovie';

export default interface IAlbum {
    currentPage: number;
    allPages: number;
    movies: IMovie[];
}
