export const removeEventListeners = (element: HTMLElement | null): void => {
    if (!element) {
        return;
    }

    const elementHTML = element.outerHTML;
    element.outerHTML = elementHTML;
};

export const loadImageAsync = (
    img: HTMLImageElement | null | undefined
): Promise<Event> => {
    return new Promise((resolve, reject) => {
        if (!img) {
            return;
        }

        img.onload = resolve;
        img.onerror = reject;
    });
};
