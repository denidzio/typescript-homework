import { IResponseMovie, IMovie } from '../interfaces';
import {
    getObjectFromLocalStorage,
    setLocalStorageItem,
} from '../helpers/localStorage.helper';

export const mapMovie = (responseMovie: IResponseMovie): IMovie => {
    return {
        id: responseMovie.id,
        overview: responseMovie.overview,
        title: responseMovie.title,
        date: responseMovie.release_date,
        posterPath: responseMovie.poster_path,
        backdropPath: responseMovie.backdrop_path,
    };
};

export const getFavouriteMoviesId = (): number[] => {
    const favouriteMovies = getObjectFromLocalStorage('favouriteMovies');

    if (!favouriteMovies) {
        return [];
    }

    return favouriteMovies;
};

export const addFavouriteMovie = (id: number): void => {
    const favouriteMovies = getFavouriteMoviesId();

    if (favouriteMovies.includes(id)) {
        return;
    }

    setLocalStorageItem('favouriteMovies', [...favouriteMovies, id]);
};

export const removeFavouriteMovie = (id: number): void => {
    const favouriteMovies = getFavouriteMoviesId();

    if (!favouriteMovies.includes(id)) {
        return;
    }

    setLocalStorageItem(
        'favouriteMovies',
        favouriteMovies.filter((movie) => movie !== id)
    );
};
