import { IAlbum, IMovieResponse } from '../interfaces';
import { mapMovie } from './movie.helper';

export const mapAlbum = (movieResponse: IMovieResponse): IAlbum => {
    return {
        currentPage: movieResponse.page,
        allPages: movieResponse.total_pages,
        movies: movieResponse.results.map(mapMovie),
    };
};
