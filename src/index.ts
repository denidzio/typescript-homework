import { renderAlbum, renderRandomMovie } from './dom/';
import { getPopularMovies } from './services/movie.service';
import { category, searcher, favourites } from './listeners';

export async function render(): Promise<void> {
    const popularMovies = await getPopularMovies();

    renderRandomMovie(popularMovies.movies);
    renderAlbum(async (page) => await getPopularMovies(page));

    searcher.listen();
    favourites.listen();
    category.listen();
}
